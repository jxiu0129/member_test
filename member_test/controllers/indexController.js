const conn = require('../config');
const successRes = require('../util/appResponse').successResponse;
const errorRes = require('../util/appResponse').errorResponse;

exports.index = (req, res) => {

  let query = "SELECT * FROM member";
  
  conn.query(query, (err, rows, fields) => {
    if(err){
      // res.json({ "status": 500, "msg": err});
      res.send(errorRes(500, err, null));
    }else{
      // res.send({
      //   "status": 200,
      //   "msg":"success",
      //   "data": rows
      // });
      res.send(successRes(200,'success', rows));
    }
  });
};

exports.create_post = (req, res) => {
  console.log('in');
  let sql = {
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    gender: req.body.gender,
    age: req.body.age,
    email: req.body.email,
    phone: req.body.phone
    };

  conn.query('INSERT INTO member SET ?', sql, (err, rows) => {
    if(err){
      console.log('db er' + err);
      // res.status(500).send('db error');
      res.send(errorRes(500, 'db error', null));
    }else{
      // res.send({
      //   "status":200,
      //   "msg":"success",
      // });
      res.send(successRes(200, 'db success', null));
      console.log('success add to db');
    }
  });
};


exports.get_delete = (req, res) => {

  let id = req.query.id;

  conn.query('DELETE FROM member WHERE member_id = ?', id, (err, rows) => {
    if(err){
      console.log(err);
      // res.send({
      //   "status":500,
      //   "msg":"fail to delete in db",
      //   "err": err
      // });
      res.send(errorRes(500, 'fail in db =>'+err, null));
    }
    // res.send({
    //   "status":200,
    //   "msg":"success add to db",
    // });
    res.send(successRes(200, 'success add to db', null));
  });
};

exports.post_update = (req, res) => {

  let id = req.body.id;
  let sql = {
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    gender: req.body.gender,
    age: req.body.age,
    email: req.body.email,
    phone: req.body.phone
  };
  console.log(sql, id);
  conn.query('UPDATE member SET ? WHERE member_id = ?', [sql, id], (err, rows) => {
    if(err){
      console.log('db er' + err);
      // res.send({
      //   "status":500,
      //   "msg": "db err",
      //   "err": err
      // });

      res.send(errorRes(500,'db error =>' + err, null));
    }
    // res.send({
    //   "status":200,
    //   "msg":"success",
    // });
    res.send(successRes(200, 'success', null));
    console.log('success change db');
  });
};


