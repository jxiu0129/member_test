exports.successResponse = (status, msg, data) => {
    let response = {
        "status": status,
        "msg": msg,
        "data": data
    };
    return response;
};

exports.errorResponse = (status, msg, data) => {
    let response = {
        "status": status,
        "msg": msg,
        "data": data
    };
    return response;
};