$(document).ready(function() {
    getMembers();
});

// ajax 抓取文章
let memberCount = 0;
function getMembers() {
    $.ajax({
    url: 'http://localhost:3000/index',
    success: (response) => {
        if (response.status != 200) {
            console.log(response.data);
        return alert('Error');
        }
        for(let i = memberCount; i < response.data.length; i++) {

        // 丟給 render function
        addMember(response.data[i]);
        memberCount++;
        }
        console.log(memberCount);
    },
    error: (err) => {
        console.log(err);
        alert('抓取失敗');
    }
    });
}

function addMember(member) {
    let table = 
        '<tr> <th scope="row">'+ member.member_id + '</th> <td>' + member.first_name +' '+ member.last_name + '</td><td>' + member.gender + '</td><td>' + member.age + '</td><td>' + member.email + '</td><td>' + member.phone+'</td>'+
        '<td><button type="button" class="btn btn-danger" onclick="Delete('+member.member_id+')">Delete</<button></td>'+
        '</tr>'+
        '</table>';
    $('.tables').append(table);
}

function Delete(id){
    let rs = confirm('Confirm to delete?');
    if(rs){
        // window.location.href = "/delete?id=" + id;
        $.ajax({
            url: "/delete?id=" + id,
            type: 'GET',
            dataType:'json',
            success: () => {
                alert('刪除成功');
                $('#table').remove();
                $('#ttable').append('<table id="table" class="table">'+
                                        '<thead>'+
                                            '<tr>'+
                                            '<th scope="col">id</th>'+
                                            '<th scope="col">Name</th>'+
                                            '<th scope="col">gender</th>'+
                                            '<th scope="col">age</th>'+
                                            '<th scope="col">email</th>'+
                                            '<th scope="col">phone</th>'+
                                            '</tr>'+
                                        '</thead>'+
                                        '<tbody class = "tables"></tbody>'+
                                    '</table>');
                memberCount = 0;
                getMembers();
            },
            error: (err) => {
                console.log('ajax err ====>', err);
            alert('ajax error');

            }
        });
    }
}


let unclick = true;
function editTable(){
    let table =
        '<div class="container" id="edittable">'+
            '<h4>編輯資料</h4>'+
            '<form>'+
                '<div class="form-group">'+
                    '<label for="id">id</label>'+
                    '<input type="text" class="form-control" id="e-id" name="id" placeholder="想改的id">'+
                '</div>'+
                '<div class="form-group">'+
                    '<label for="last-name">姓</label>'+
                    '<input type="text" class="form-control" id="e-last-name" name="last_name" placeholder="想改的姓">'+
                '</div>'+
                '<div class="form-group">'+
                    '<label for="first-name">名</label>'+
                    '<input type="text" class="form-control" id="e-first-name" name="first_name" placeholder="想改的名">'+
                '</div>'+
                '<div class="form-group">'+
                    '<label for="gender">性別</label>'+
                    '<select class="form-control" id="e-gender" name="gender">'+
                    '<option>M</option>'+
                    '<option>F</option>'+
                    '</select>'+
                '</div>'+
                '<div class="form-group">'+
                    '<label for="age">年齡</label>'+
                    '<input type="text" class="form-control" id="e-age" name="age" placeholder="想改的年紀">'+
                '</div>'+
                '<div class="form-group">'+
                    '<label for="email">信箱</label>'+
                    '<input type="email" class="form-control" id="e-email" name="email" placeholder="想改的email">'+
                '</div>'+
                '<div class="form-group">'+
                    '<label for="phone">電話</label>'+
                    '<input type="text" class="form-control" id="e-phone" name="phone" placeholder="想改的電話">'+
                '</div>'+
                '<p type="submit" class="btn btn-danger" onclick="sendEdit()">send</p>'+
            '</form>'+
        '</div>';
    if (unclick){
        $('.edit-table').append(table);
        console.log(unclick);
        unclick = false;
    } else {
        $('#edittable').remove();
        unclick = true;
    }
}

function sendEdit(){
    $.ajax({
        url: 'http://localhost:3000/update',
        type: 'POST',
        dataType:'json',
        data:{
            "id": $('#e-id').val(),
            "first_name": $('#e-first-name').val(),
            "last_name": $('#e-last-name').val(),
            "gender": $('#e-gender').val(),
            "age": $('#e-age').val(),
            "email": $('#e-email').val(),
            "phone": $('#e-phone').val(),
        },
        success: (res) => {
            console.log('success ajax');
            if(res.status === 200){
                console.log(res);
                alert('更改成功');
                $('#table').remove();
                $('#ttable').append('<table id="table" class="table">'+
                                        '<thead>'+
                                            '<tr>'+
                                            '<th scope="col">id</th>'+
                                            '<th scope="col">Name</th>'+
                                            '<th scope="col">gender</th>'+
                                            '<th scope="col">age</th>'+
                                            '<th scope="col">email</th>'+
                                            '<th scope="col">phone</th>'+
                                            '</tr>'+
                                        '</thead>'+
                                        '<tbody class = "tables"></tbody>'+
                                    '</table>');
                memberCount = 0;
                getMembers();
            }
            if(res.status === 500){
                alert(res.msg);
                console.log(res.msg);
            }
        },
        error: (err) => {
            console.log('ajax err ====>', err);
            alert('ajax error');
        }
    });
}

function validate(){
    $.ajax({
        url: 'http://localhost:3000/create',
        type:'POST',
        dataType:'json',
        data:{
            "first_name": $('#first-name').val(),
            "last_name": $('#last-name').val(),
            "gender": $('#gender').val(),
            "age": $('#age').val(),
            "email": $('#email').val(),
            "phone": $('#phone').val(),
        },
        success: (res) => {
            console.log('success ajax');
            if(res.status === 200){
                console.log(res);
                alert('新增成功');
                // $('.tables').remove()
                getMembers();
            }
            if(res.status === 500){
                console.log(res);
                alert(res.msg);
                console.log(res.err);
            }
        },
        error: (err) => {
            console.log('ajax err ====>', err);
            alert('ajax error');
        }
    });
}
