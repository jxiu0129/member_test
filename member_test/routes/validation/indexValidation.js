const Joi = require('joi');
const errorRes = require('../../util/appResponse').errorResponse;

exports.addSchema = (req, res, next) => {
    try{
        let schema = Joi.object/*().keys*/({
            id: Joi.number(),
            first_name: Joi.string().trim().required(),
            last_name: Joi.string().trim().required(),
            gender: Joi.string(),
            age: Joi.number(),
            email: Joi.string().trim().email().required(),
            phone: Joi.string().min(10).max(10)
          });
    
          console.log('req.body =>' + req.body);
          Joi.validate(req.body, schema)
            .then((result)=>{
                console.log('success validate, success =>' + result);
                next();
                })
            .catch( async (err)=>{
                await console.log('validate err => ' + err);
                // res.send({
                //     "status":500,
                //     "msg":"validation error",
                //     "err":err
                // });
                res.send(errorRes(500, 'validation error => ' + err, null));
                
            });
    }catch (err){
        console.log(err);
    }
};
