var express = require('express');
var router = express.Router();

const index_controller = require('../controllers/indexController');
const index_validator = require('./validation/indexValidation');

// index
router.get('/index', index_controller.index);

// create
router.post('/create', index_validator.addSchema, index_controller.create_post);

// delete
router.get('/delete', index_controller.get_delete);

// update
router.post('/update',index_validator.addSchema, index_controller.post_update);


module.exports = router;
